package com.issues.api;

import com.issues.utils.ConfigFileReader;
import com.issues.utils.Helper;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalToIgnoringCase;

public class IssuesApi {
    private ConfigFileReader objconfig = new ConfigFileReader();
    private Helper objHelper = new Helper();
    private String accessToken;
    private JsonPath jsonPath;
    private String issueIid;

    //Method to get access token to authorize all other API and this method will run at that start of the suite
    @BeforeSuite
    public void getAccessToken() {
        Response response = given()
                .formParam("grant_type", "password")
                .formParam("username", objconfig.getUsername())
                .formParam("password", objconfig.getPassword())
                .when()
                .post(objconfig.getAuthenticationUrl());
        JsonPath jsonPathEvaluator = response.jsonPath();
        accessToken = jsonPathEvaluator.getString("access_token");
    }

    //Method create new project issue with random alphanumeric title
    //Scenario of Successful operation of create issue for specific project
    @Test
    public void createNewIssues() {
        RestAssured.baseURI = objconfig.getBaseUrl();
        Response response = given().auth().preemptive().oauth2(accessToken).and()
                .queryParam("title", objHelper.getAlphanumericString(8))
                .pathParam("projectId", objconfig.getProjectId())
                .when().post(IssuesApiResources.createIssueResource());
        Assert.assertEquals(response.statusCode(), 201);
        jsonPath = new JsonPath(response.asString());
        issueIid = jsonPath.getString("iid");
        Assert.assertNotNull(issueIid);
        //Validate json schema from the response json with the expected json schema stored in json file under resources folder
        response.then().assertThat().body(matchesJsonSchemaInClasspath("CreateIssue.json"));
    }

    //Method reads all the issues for which user has access
    @Test
    public void readAllIssues() {
        RestAssured.baseURI = objconfig.getBaseUrl();
        given().auth().preemptive().oauth2(accessToken).and()
                .pathParam("projectId", objconfig.getProjectId())
                .when().get(IssuesApiResources.readIssuesResource())
                .then().statusCode(200);
    }

    //Method edit existing issue based on internal id of a project's issue(iid) and we choose title to update the issue
    //Successful operation where user update existing project issue based on issue id
    @Test
    public void updateExistingIssue() {
        String titleToBeUpdated = objHelper.getAlphanumericString(8);
        RestAssured.baseURI = objconfig.getBaseUrl();
        given().auth().preemptive().oauth2(accessToken).and()
                .queryParam("title", titleToBeUpdated)
                .pathParam("projectId", objconfig.getProjectId())
                .pathParam("issueIid", issueIid)
                .when().put(IssuesApiResources.updateIssueResource())
                .then().statusCode(200)
                .body("title", equalToIgnoringCase(titleToBeUpdated));

    }

    //Method delete existing issue based on internal id of a project's issue(iid)
    //Successful scenario where user delete existing project issue based on issue id
    @Test
    public void deleteExistingIssue() {
        RestAssured.baseURI = objconfig.getBaseUrl();
        given().auth().preemptive().oauth2(accessToken).and()
                .pathParam("projectId", objconfig.getProjectId())
                .pathParam("issueIid", issueIid)
                .when().delete(IssuesApiResources.deleteIssueResource())
                .then().statusCode(204);
    }

    //Method moves an issue to a different project based on internal id of a project's issue(iid) and the ID of the new project(to_project_id)
    //Testing edge case here that user can not move the issue to the same project
    @Test
    public void moveIssueInSameProject() {
        RestAssured.baseURI = objconfig.getBaseUrl();
        given().auth().preemptive().oauth2(accessToken).and()
                .pathParam("projectId", objconfig.getProjectId())
                .pathParam("issueIid", objHelper.getDataFromJson("moveIssueId"))
                .when().post(IssuesApiResources.moveIssueResource())
                .then().statusCode(400)
                .body("error", equalToIgnoringCase("to_project_id is missing"));
    }

    //Method subscribe user to an issue to receive notification based on internal id of a project's issue(iid)
    //Testing edge scenario here that user can not subscribe to an issue for which he/she is already subscribed
    @Test
    public void subscribeIssueWhichIsAlreadySubscribed() {
        RestAssured.baseURI = objconfig.getBaseUrl();
        given().auth().preemptive().oauth2(accessToken).and()
                .pathParam("projectId", objconfig.getProjectId())
                .pathParam("issueIid", objHelper.getDataFromJson("subscribeIssueId"))
                .when().post(IssuesApiResources.subscribeIssueResource())
                .then().statusCode(304);
    }

    //Method unsubscribe user to an issue to not receive notification based on internal id of a project's issue(iid)
    //Testing edge scenario here that user can not unsubscribe to an issue for which he/she is already unsubscribed
    @Test
    public void unsubscribeIssueWhichIsAlreadyUnsubscribed() {
        RestAssured.baseURI = objconfig.getBaseUrl();
        given().auth().preemptive().oauth2(accessToken).and()
                .pathParam("projectId", objconfig.getProjectId())
                .pathParam("issueIid", objHelper.getDataFromJson("unsubscribeIssueId"))
                .when().post(IssuesApiResources.unsubscribeIssueResource())
                .then().statusCode(304);
    }

    @Test
    //Method tries to create issue without title which is mandatory field- unsuccessful operation
    public void createIssueWithoutTitle() {
        RestAssured.baseURI = objconfig.getBaseUrl();
        given().auth().preemptive().oauth2(accessToken).and()
                .pathParam("projectId", objconfig.getProjectId())
                .when().post(IssuesApiResources.createIssueResource())
                .then().statusCode(400)
                .body("error", equalToIgnoringCase("title is missing"));
    }

    @Test
    //Method creates an to-do for the user on an issue based on internal id of a project's issue(iid)
    //Testing edge scenario here that user tried to create to-do on the issue for which to-do already there
    public void createTodoForIssueOnWhichTodoAlreadyExist() {
        RestAssured.baseURI = objconfig.getBaseUrl();
        given().auth().preemptive().oauth2(accessToken).and()
                .pathParam("projectId", objconfig.getProjectId())
                .pathParam("issueIid", objHelper.getDataFromJson("todoIssueId"))
                .when().post(IssuesApiResources.todoForIssueResource())
                .then().statusCode(304);
    }
}
