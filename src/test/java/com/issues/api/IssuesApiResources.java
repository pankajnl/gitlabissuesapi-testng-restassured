package com.issues.api;

public class IssuesApiResources {
    //Method provide resource url of create issue API
    public static String createIssueResource() {
        String createIssueResource = "/projects/{projectId}/issues";
        return createIssueResource;
    }

    //Method provide resource url of update issue API
    public static String updateIssueResource() {
        String updateIssueResource = "/projects/{projectId}/issues/{issueIid}";
        return updateIssueResource;
    }

    //Method provide resource url of read issue API
    public static String readIssuesResource() {
        String readIssueResource = "/projects/{projectId}/issues";
        return readIssueResource;
    }

    //Method provide resource url of delete issue API
    public static String deleteIssueResource() {
        String deleteIssueResource = "/projects/{projectId}/issues/{issueIid}";
        return deleteIssueResource;
    }

    //Method provide resource url of move issue API
    public static String moveIssueResource() {
        String moveIssueResource = "/projects/{projectId}/issues/{issueIid}/move";
        return moveIssueResource;
    }

    //Method provide resource url of subscribe issue API
    public static String subscribeIssueResource() {
        String subscribeIssueResource = "/projects/{projectId}/issues/{issueIid}/subscribe";
        return subscribeIssueResource;
    }

    //Method provide resource url of unsubscribe issue API
    public static String unsubscribeIssueResource() {
        String unsubscribeIssueResource = "/projects/{projectId}/issues/{issueIid}/unsubscribe";
        return unsubscribeIssueResource;
    }

    //Method provide resource url of to-do for issue API
    public static String todoForIssueResource() {
        String todoForIssueResource = "/projects/{projectId}/issues/{issueIid}/todo";
        return todoForIssueResource;
    }
}
