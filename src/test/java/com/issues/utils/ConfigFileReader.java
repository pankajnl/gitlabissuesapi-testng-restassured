package com.issues.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {
    Properties objproperties = new Properties();
    FileInputStream objfis;
    private String filePath = "src\\test\\resources\\Configuration.properties";

    //Constructor will get call as soon as object of this class initiate and eventually which will help to load all the properties from property file in property object
    public ConfigFileReader() {
        try {
            objfis = new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            objproperties.load(objfis);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Method provide authentication url from configuration.properties file
    public String getAuthenticationUrl() {
        String ouathUrl = objproperties.getProperty("ouathUrl");
        if (ouathUrl != null) return ouathUrl;
        else throw new RuntimeException("ouathUrl not specified in the Configuration.properties file.");
    }

    //Method provide base api url from configuration.properties file
    public String getBaseUrl() {
        String baseUrl = objproperties.getProperty("baseUrl");
        if (baseUrl != null) return baseUrl;
        else throw new RuntimeException("baseUrl not specified in the Configuration.Properties file");
    }

    //Method provide username for authentication purpose from configuration.properties file
    public String getUsername() {
        String username = objproperties.getProperty("username");
        if (username != null) return username;
        else throw new RuntimeException("Username not specified in the Configuration.Properties file");
    }

    //Method provide password for authentication purpose from configuration.properties file
    public String getPassword() {
        String password = objproperties.getProperty("password");
        if (password != null) return password;
        else throw new RuntimeException("Password not specified in the Configuration.Properties file");
    }

    //Method provide project id from configuration.properties file
    public String getProjectId() {
        String projectId = objproperties.getProperty("projectId");
        if (projectId != null) return projectId;
        else throw new RuntimeException("ProjectId not specified in the Configuration.Properties file");
    }
}
