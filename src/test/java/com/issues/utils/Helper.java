package com.issues.utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class Helper {
    //This method is used to generate random alphanumeric string based on length provided by user.
    //Purpose to use random string is if user try to create issue with the same title then after few attempt api gives 400 bad request error
    public String getAlphanumericString(int stringLenght) {

        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        StringBuilder sb = new StringBuilder(stringLenght);
        for (int i = 0; i < stringLenght; i++) {
            int index = (int) (AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }

    //This method is used to provide data from json based on received request from step definition or api so that user does not have to hard code any data in api test
    public String getDataFromJson(String requiredData) {
        String jsonPath = "src\\test\\resources\\TestData.json";
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = null;
        try {
            jsonObject = jsonParser.parse(new FileReader(jsonPath)).getAsJsonObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String data = jsonObject.get(requiredData).getAsString();
        return data;

    }
}
