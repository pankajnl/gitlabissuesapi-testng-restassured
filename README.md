# GitLab API TestNG-Rest assured

The Project contains automated GitLab project issues test scenarios. Scenarios are CRUD operation on issues API and few edge case scenarios.

## Getting Started

To work with the project please check out the master branch and store the project on local machine. See Running the tests section to run all the scenarios.

### Prerequisites

Below tools should be installed on local machine

```
1) Idea like IntelliJ/Android studio/Eclipse
2) Maven build tool
3) Java SE Development Kit(jdk)
```

### Installing

A step by step series of instruction to get the project working on your local machine

```
1) Launch the development idea like Intellij/Android Studio/Eclipse
2) Open the project from the location where it has been stored during checkout
3) Enable auto-import of maven dependancy
```



## Running the tests

Test scenarios can be run in two different ways as explained below

```
1) Run the testng.xml file from idea to run all the scenarios
2) In the terminal or cmd traverse till the project path and run the command "mvn clean test"
To run above command first command maven path should be stored under environment variable please follow https://maven.apache.org/install.html for more reference.
```



## Results

Test results are stored under test-output folder in HTML format and file name is ExtentReportsTestNG



## Built With

- [REST-assured](http://rest-assured.io/) - Java library
- [Maven](https://maven.apache.org/) - Build tool
- [TestNG](https://testng.org/doc/) - Testing Framework

## Authors

- **Pankaj Nalawade**

  